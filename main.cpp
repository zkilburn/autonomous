/*****************************************************************************************
* Name : Fast object tracking using the OpenCV library *
* Author : Lior Chen <chen.lior@gmail.com> *
* Notice : Copyright (c) Jun 2010, Lior Chen, All Rights Reserved *
* : *
* Site : http://www.lirtex.com *
* WebPage : http://www.lirtex.com/robotics/fast-object-tracking-robot-computer-vision *
* : *
* Version : 1.0 *
* Notes : By default this code will open the first connected camera. *
* : In order to change to another camera, change *
* : CvCapture* capture = cvCaptureFromCAM( 0 ); to 1,2,3, etc. *
* : Also, the code is currently configured to tracking RED objects. *
* : This can be changed by changing the hsv_min and hsv_max vectors *
* : *
* License : This program is free software: you can redistribute it and/or modify *
* : it under the terms of the GNU General Public License as published by *
* : the Free Software Foundation, either version 3 of the License, or *
* : (at your option) any later version. *
* : *
* : This program is distributed in the hope that it will be useful, *
* : but WITHOUT ANY WARRANTY; without even the implied warranty of *
* : MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the *
* : GNU General Public License for more details. *
* : *
* : You should have received a copy of the GNU General Public License *
* : along with this program. If not, see <http://www.gnu.org/licenses/> *
******************************************************************************************/

#include <opencv/cvaux.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>
#include <stdio.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>

using namespace cv;

 //--------------------------------------------------------------------------------------------------------------------   
void moveRight()
{
	printf("Right------> \n");
	return;
}
void moveLeft()
{
	printf("<<------Left \n");
	return;
}
 //--------------------------------------------------------------------------------------------------------------------   
void imageCentered()
{
	printf("<<--Your CENTERED-->> \n");
	return;
}


// test if 3 wide array is full of zeros or not... used to detect empty points
bool emptyArray(float testArray[3]){
        if(testArray[0]==0 && testArray[1]==0 && testArray[2]==0){
                return true;
        }else return false;
}

float findDist(int x){
	if(x<0)
	{
        x=x*(-1);
    }
	return 255.94*pow(x,-1.007);
}



void setEqual(float a[3],float b[3]){
        
        for (int k=0;k<3;k++){
                a[k]=b[k];
        }
        return;
}
void wipeArray(float a[3]){
        for (int i=0;i<3;i++){
                a[i]=0;
        }
        return ;
}
bool samePoints(float*a,float*b,int tolerance){
	if(a[0]<=b[0]+tolerance && a[0]>=b[0]-tolerance)
	{
		if(a[1]<=b[1]+tolerance && a[1]>=b[1]-tolerance){
			return true;
		}
	}else return false;
}

bool validPoints(float *a, float* b,float* c)
{
        if(samePoints(a,b,50))
		{
        	return false;
        }else if(samePoints(a,c,50))
        {
        	return false;
        }else if(samePoints(b,c,50))
        {
        	return false;
        }else return true;
                        
                       
        
}

int doCalc(float *p1, float *p2, float * p3){
	int screenCenter=320;
	float vertdist = p1[1]-p2[1];
    float y=findDist(vertdist);
	float hordist = p1[0]-p3[0];
    
    //Calculates the midpoint of the x LED's and uses submethods to relocate our signature 
    //to the center of the screen by comparing the x coords to the actual x midpoint of screen
	int xCoords =(cvRound(p1[0])+cvRound(p3[0]))/2;
    
    //---------------------------------------------------------------------------------------------
    //attempting some theta calculation based on LED distance in the horizontal plane                          
    if(hordist<0)
	{
    	hordist=hordist*(-1);
    }                                
	int x=hordist;                                
    if(vertdist>50&&hordist>50)
    {
    
     printf("found vertical distance y= %f \n", vertdist);
	printf("horizontal x=%f \n", hordist);
	printf("distance to sensor (feet)= %f \n", y);                               
    
	if(xCoords<screenCenter)
	{
    	moveRight();
    	return 1;
    } else if(xCoords>screenCenter)
    {
    	moveLeft();
    	return 1;
    }else if(xCoords==screenCenter)
    {
    	imageCentered();
    	return 2; 
    }
	}else return 0;
}


// formula for printing out x coords and y coords from our arrays of circle information
int printPoints(float * a, float *b , float * c)
{
		int detectionthresh=20;		
		//a and b are vertical ... a and c are horizontal
        if (cvRound(a[0])<=(cvRound(b[0])+detectionthresh) && cvRound(a[0])>=(cvRound(b[0]))-detectionthresh)
                        {
                                return doCalc(a,b,c);                                
						//b and c are vertical... a and c are horizontal
                        }else if (cvRound(b[0])<=(cvRound(c[0])+detectionthresh) && cvRound(b[0])>=(cvRound(c[0]))-detectionthresh)
                        {                                
								return doCalc(b,c,a);								
                        //a and c are verticcal.... a and b are horizontal
                        }else if (cvRound(a[0])<=(cvRound(c[0])+detectionthresh) && cvRound(a[0])>=(cvRound(c[0]))-detectionthresh)
                        {
                        		doCalc(a,c,b);                                
                        }
                        return 0;
                }        

void pathToGoal(int current[2],int goal[2])
{
	//ideal route
	//while curr!=goal
	//match y
	// match x
	
	//actual route
	//while curr!=goal
		// test sensors for object
		//if no adjacent objects 
			//match x and y
		//if object present
			//navigate to other side of object
				//change x/y precedence... move
				// if object is inline x
					//change goal y -1+1
					//pass object
					//correct y goal
				// if object is inline y
					//change goal x-1+1
					//pass object
					//correct goal x
		//continue;
	
}

int main(int argc, char* argv[])
{
        //Limits on the threshholds
        int maxRed = 256, minRed = 250;
        int maxGrn = 256, minGrn = 250;
        int maxBlu = 256, minBlu =250;
        //int maxH = 165, minH = 155;
        //int maxS = 500, minS = 0;
        //int maxL = 245, minL =235;
        
        // Default capture size - 640x480
        CvSize size = cvSize(640,480);
        
        // Open capture device. 0 is /dev/video0, 1 is /dev/video1, etc.
        CvCapture* capture = cvCaptureFromCAM(0);
        if( !capture )
        {
                fprintf( stderr, "ERROR: capture is NULL \n" );
                getchar();
                return -1;
        }
		        
        // Create a window in which the captured images will be presented
        cvNamedWindow("Camera", CV_WINDOW_AUTOSIZE );
        //cvNamedWindow("RGB", CV_WINDOW_AUTOSIZE );
        //cvNamedWindow("Adjustment", CV_WINDOW_AUTOSIZE );
        //cvNamedWindow("A2",CV_WINDOW_AUTOSIZE);
        //cvNamedWindow("Test Output",CV_WINDOW_AUTOSIZE);
        
        //Create the trackbars to control the threshold values
        //cvCreateTrackbar("Max RED", "Adjustment", &maxRed, 500, NULL);
//        cvCreateTrackbar("Min RED", "Adjustment", &minRed, 500, NULL);
//        cvCreateTrackbar("Max GREEN", "Adjustment", &maxGrn, 500, NULL);
//        cvCreateTrackbar("Min GREEN", "Adjustment", &minGrn, 500, NULL);
//        cvCreateTrackbar("Max BLUE", "Adjustment", &maxBlu, 500, NULL);
//        cvCreateTrackbar("Min BLUE", "Adjustment", &minBlu, 500, NULL);
//        
//        cvCreateTrackbar("Max H", "A2", &maxH, 500, NULL);
//        cvCreateTrackbar("Min H", "A2", &minH, 500, NULL);
//        cvCreateTrackbar("Max S", "A2", &maxS, 500, NULL);
//        cvCreateTrackbar("Min S", "A2", &minS, 500, NULL);
//        cvCreateTrackbar("Max L", "A2", &maxL, 500, NULL);
//        cvCreateTrackbar("Min L", "A2", &minL, 500, NULL);
        
        IplImage * rgb_frame = cvCreateImage(size, IPL_DEPTH_8U, 3);
        IplImage* thresholded = cvCreateImage(size, IPL_DEPTH_8U, 1);
        //IplImage* hls_frame = cvCreateImage(size,IPL_DEPTH_8U, 3);
        //IplImage* thresholded2 = cvCreateImage(size, IPL_DEPTH_8U, 1);
                
        //IMAGE PROCESSING
        
        //currentProcess decides the autonomous loop functionality
        //0 = seen nothing, generally look
        //1 = centering
        //2 = centered and found
    	int currentProcess=0;
        
        while(currentProcess<2)
        {        	
 				//--------------------------------------------------------------------------------------------------------------------       	
        		if(currentProcess=0){
        			//insert the look around aimlessless code
        		}   	
  				//--------------------------------------------------------------------------------------------------------------------          	
        	    // Get one frame
                IplImage* frame = cvQueryFrame( capture );
                if(!frame)
                {
                        fprintf( stderr, "ERROR: frame is null...\n" );
                        getchar();
                        break;
                }
                
                // Detect a red ball
                CvScalar rgb_min = cvScalar(minRed, minGrn, minBlu, 0);
                CvScalar rgb_max = cvScalar(maxRed, maxGrn, maxBlu, 0);
                
                //CvScalar hls_min =cvScalar(minH,minL,minS,0);
                //CvScalar hls_max =cvScalar(maxH,maxL,maxS,0);
                
                // Covert color space to HSV as it is much easier to filter colors in the HSV color-space.
                //Given our camera image 'frame' -- outs a revised separate image 'hsv_frame'
                cvCvtColor(frame, rgb_frame, CV_BGR2RGB);
                //cvCvtColor(frame, hls_frame,CV_RGB2HLS);
                
                // Filter out colors which are out of range.
                //takes in revised 'hsv_frame' and lower and upper bounds -- outs a revised 'image thresholded'
                cvInRangeS(rgb_frame, rgb_min, rgb_max, thresholded);
                //cvInRangeS(hls_frame, hls_min, hls_max, thresholded2);
                
                // Memory for hough circles
                //Creating a storage 
                CvMemStorage* storage = cvCreateMemStorage(0);
                
                // hough detector works better with some smoothing of the image
                cvSmooth( thresholded, thresholded, CV_GAUSSIAN, 13, 13 );
                
                //Creates circles array with locations of ball 
                CvSeq* circles = cvHoughCircles(thresholded, storage, CV_HOUGH_GRADIENT, 2,
                                                                                thresholded->height/120, 50 , 14, 1, 15);       
                
                //we found three circles
                float comparison1[3];
                float comparison2[3];
                float comparison3[3];
                if(circles->total == 3)
                {
                        float * a = (float*)cvGetSeqElem(circles, 0);
                        float * b = (float*)cvGetSeqElem(circles, 1);
                        float * c = (float*)cvGetSeqElem(circles, 2);
                                                       
                        currentProcess=printPoints(a,b,c);                                     
                } 
				
                //Parses the created circles array to get valid information and overlay these circles onto our 'frame' from camera
                for (int i = 0; i < circles->total; i++)
                {
                        float* p = (float*)cvGetSeqElem( circles, i );
                        cvCircle( frame, cvPoint(cvRound(p[0]),cvRound(p[1])),
                                          3, CV_RGB(0,255,0), -1, 8, 0 );
                        cvCircle( frame, cvPoint(cvRound(p[0]),cvRound(p[1])),
                        cvRound(p[2]), CV_RGB(255,0,0), 3, 8, 0 ); 
                }
                
                //OUTPUTS of our images
                cvShowImage("Camera", frame ); // Original stream with detected ball overlay
//				cvShowImage("RGB", rgb_frame); // Original stream in the HSV color space
//              cvShowImage("After Color Filtering", thresholded ); // The stream after color filtering
//              cvShowImage("Test Output",thresholded2);
                cvReleaseMemStorage(&storage);
                
                // Do not release the frame!
                
                //If ESC key pressed, Key=0x10001B under OpenCV 0.9.7(linux version),
                //remove higher bits using AND operator
                if( (cvWaitKey(10) & 255) == 27 ) break;
        }
        
        
      //--------------------------------------------------------------------------------------------------------------------     
	  
	  	//mapping grid
        bool grid[39][74];
        for (int i=0;i<39;i++)
        {
        	for (int j=0;j<74;j++)
        	{ 		
        		grid[i][j]=true;
        		
        		
        	}
        } 
	   
        int currentPosition[2];
        int goal[2];
        //direction to move now, 0 backwards, 1 forwards, 2, left, 3 right
        int direction;
        
        if(goal[1]=currentPosition[1])
		{
        	//in line y
        	//check ifchange in x is valid
        	if(goal[0]=currentPosition[0]){
        		printf("you have reached destination");
        	}else if(currentPosition[0]<goal[0]){
        		if(grid[currentPosition[0]+1][currentPosition[1]]==true){
        			//move x+1
        		}else 
        		{
        			//while x-1 ==false
        			//move y+1 
        			
        			//move x+2
        		}
        	}else if (currentPosition[0]>goal[0])
        	{
        		if(grid[currentPosition[0]-1][currentPosition[1]]==true){
        			//move x-1
        		}else 
        		{
        			//while x-1 ==false
        			//move y+1
					
        			//move x+2
        			
        		}
			}
        	
        }else if(goal[1]<currentPosition[1])
        {
        	//move y+1
        	
        }else if(goal[1]>currentPosition[1])
        {
        	//move y-1
        }
        //--------------------------------------------------------------------------------------------------------------------    
        // Release the capture device housekeeping
        cvReleaseCapture( &capture );
        cvDestroyWindow( "mywindow" );   
        
        return 0; 
        
         
        
        
}
